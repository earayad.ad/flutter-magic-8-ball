import 'package:flutter/material.dart';
import 'dart:math';
void main() => runApp(MagicBall());

class MagicBall extends StatefulWidget {
  @override
  State<MagicBall> createState() => MagicBall_State();
}

class MagicBall_State extends State<MagicBall> {
  int ballNumber = 1;

  void changeBall() {
    setState(() {
      ballNumber = Random().nextInt(5) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          title: Text('Ask Me Anything'),
          backgroundColor: Colors.blue[900],
        ),
        body: Center(
          child: TextButton(
            onPressed: changeBall,
            child: Container(
              padding: EdgeInsets.all(16.0),
              child: Image.asset('images/ball$ballNumber.png'),
            ),
          ),
        ),
      )
    );
  }
}

